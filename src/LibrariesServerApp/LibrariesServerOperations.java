package LibrariesServerApp;


/**
* LibrariesServerApp/LibrariesServerOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from D:/DLMS_CORBA/DLMS_CORBA/src/corbainterface/LibrariesServer.idl
* Thursday, February 28, 2019 8:38:15 o'clock PM EST
*/

public interface LibrariesServerOperations 
{
  String addItem (String managerID, String itemID, String itemName, int quantity);
  String removeItem (String managerID, String itemID, int quantity);
  String listItemAvailability (String managerID);
  String borrowItem (String userID, String itemID);
  String findItem (String userID, String itemName, boolean flag);
  String returnItem (String userID, String itemID);
  String waitingQueueList (String userID, String itemID);
  String exchangeItem (String userID, String newItemID, String oldItemID);
} // interface LibrariesServerOperations
