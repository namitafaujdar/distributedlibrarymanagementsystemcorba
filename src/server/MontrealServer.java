package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import LibrariesServerApp.LibrariesServer;
import LibrariesServerApp.LibrariesServerHelper;
import constants.ConstantValues;
import serverInterfaceImplementation.MontrealLibraryImplementation;

/**
 * 
 * @author Namita Faujdar
 *
 */

public class MontrealServer {

	public static void main(String[] args) throws Exception{

		MontrealLibraryImplementation monStub = new MontrealLibraryImplementation();
		Runnable task = () -> {
				receiveRequestsFromOthers(monStub);
		};
		
		Runnable task1 = () -> {
				handleMontrealRequests(monStub,args);
		};
		
		Thread thread = new Thread(task);
		Thread thread1 = new Thread(task1);
		thread.start();
		thread1.start();

		}

	private static void receiveRequestsFromOthers(MontrealLibraryImplementation monStub) {
		DatagramSocket aSocket = null;
		try {
			aSocket = new DatagramSocket(ConstantValues.MON_SERVER_PORT);
			System.out.println("Montreal server started.....");
			while (true) {
				byte[] buffer = new byte[1000];
				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(request);
				String response = requestsFromOthers(new String(request.getData()), monStub);
				DatagramPacket reply = new DatagramPacket(response.getBytes(), response.length(), request.getAddress(),
						request.getPort());
				aSocket.send(reply);
			}
		} catch (IOException e) {
			System.out.println("IO: " + e.getMessage());
		} finally {
			if (aSocket != null)
				aSocket.close();
		}
	}

	private static void handleMontrealRequests(MontrealLibraryImplementation monImpl, String[] args) {
		ORB orb = ORB.init(args, null);
		try {
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			
			monImpl.setOrb(orb);
			
			LibrariesServer href = LibrariesServerHelper.narrow(rootPoa.servant_to_reference(monImpl));
			
			NamingContextExt namingContextReference = NamingContextExtHelper.narrow(orb.resolve_initial_references("NameService"));
			NameComponent[] path = namingContextReference.to_name(ConstantValues.MON_SERVER_NAME);
			
			namingContextReference.rebind(path, href);
			System.out.println("Montreal Server is ready");
			while(true) {
				orb.run();
			}
			
		} catch (InvalidName | AdapterInactive | org.omg.CosNaming.NamingContextPackage.InvalidName | ServantNotActive | WrongPolicy | NotFound | CannotProceed e) {
			System.out.println("Something went wrong in montreal server: "+e.getMessage());
		}
		
	}

	
	public static String requestsFromOthers(String data, MontrealLibraryImplementation monLibraryImplementation) {
			String[] receivedDataString = data.split(" ");
			String userId = receivedDataString[0].trim();
			String itemId = receivedDataString[1].trim();
			String methodNumber = receivedDataString[2].trim();
			String itemName = receivedDataString[3].trim();
			String oldItemId = receivedDataString[4].trim();
			switch(methodNumber) {
			case "1": return monLibraryImplementation.findItem(userId, itemName, false);
			case "2": return monLibraryImplementation.borrowItem(userId, itemId); 
			case "3": return monLibraryImplementation.returnItem(userId, itemId); 
			case "4": return monLibraryImplementation.waitingQueueList(userId, itemId);
			case "5": return monLibraryImplementation.exchangeItem(userId, itemId, oldItemId);
			case "6": return monLibraryImplementation.borrowedBookOrNot(userId, itemId, oldItemId);
			case "7": return monLibraryImplementation.bookExistOrNot(userId, itemId, oldItemId);
			}
		return "Incorrect";
	}
}