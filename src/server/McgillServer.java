package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import LibrariesServerApp.LibrariesServer;
import LibrariesServerApp.LibrariesServerHelper;
import constants.ConstantValues;
import serverInterfaceImplementation.McgillLibraryImplementation;

/**
 * 
 * @author Namita Faujdar
 *
 */

public class McgillServer {

	public static void main(String[] args) throws Exception{

		McgillLibraryImplementation mcgStub = new McgillLibraryImplementation();
		Runnable task = () -> {
				receiveRequestsFromOthers(mcgStub);
		};
		
		Runnable task1 = () -> {
				handleMcgillRequests(mcgStub,args);
		};
		
		Thread thread = new Thread(task);
		Thread thread1 = new Thread(task1);
		thread.start();
		thread1.start();

		}

	private static void receiveRequestsFromOthers(McgillLibraryImplementation mcgStub) {
		DatagramSocket aSocket = null;
		try {
			aSocket = new DatagramSocket(ConstantValues.MCG_SERVER_PORT);
			System.out.println("McGill server started.....");
			while (true) {
				byte[] buffer = new byte[1000];
				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(request);
				String response = requestsFromOthers(new String(request.getData()), mcgStub);
				DatagramPacket reply = new DatagramPacket(response.getBytes(), response.length(), request.getAddress(),
						request.getPort());
				aSocket.send(reply);
			}
		} catch (IOException e) {
			System.out.println("IO: " + e.getMessage());
		} finally {
			if (aSocket != null)
				aSocket.close();
		}
	}

	private static void handleMcgillRequests(McgillLibraryImplementation mcgImpl, String[] args) {
		ORB orb = ORB.init(args, null);
		try {
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			
			mcgImpl.setOrb(orb);
			
			LibrariesServer href = LibrariesServerHelper.narrow(rootPoa.servant_to_reference(mcgImpl));
			
			NamingContextExt namingContextReference = NamingContextExtHelper.narrow(orb.resolve_initial_references("NameService"));
			NameComponent[] path = namingContextReference.to_name(ConstantValues.MCG_SERVER_NAME);
			
			namingContextReference.rebind(path, href);
			System.out.println("Mcgill Server is ready");
			while(true) {
				orb.run();
			}
			
		} catch (InvalidName | AdapterInactive | org.omg.CosNaming.NamingContextPackage.InvalidName | ServantNotActive | WrongPolicy | NotFound | CannotProceed e) {
			System.out.println("Something went wrong in mcgill server: "+e.getMessage());
		}
		
	}

	
	public static String requestsFromOthers(String data, McgillLibraryImplementation mcgLibraryImplementation) {
			String[] receivedDataString = data.split(" ");
			String userId = receivedDataString[0].trim();
			String itemId = receivedDataString[1].trim();
			String methodNumber = receivedDataString[2].trim();
			String itemName = receivedDataString[3].trim();
			String oldItemId = receivedDataString[4].trim();
			switch(methodNumber) {
			case "1": return mcgLibraryImplementation.findItem(userId, itemName, false);
			case "2": return mcgLibraryImplementation.borrowItem(userId, itemId); 
			case "3": return mcgLibraryImplementation.returnItem(userId, itemId); 
			case "4": return mcgLibraryImplementation.waitingQueueList(userId, itemId);
			case "5": return mcgLibraryImplementation.exchangeItem(userId, itemId, oldItemId);
			case "6": return mcgLibraryImplementation.borrowedBookOrNot(userId, itemId, oldItemId);
			case "7": return mcgLibraryImplementation.bookExistOrNot(userId, itemId, oldItemId);
			}
		return "Incorrect";
	}
}
